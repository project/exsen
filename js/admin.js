(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.exsen = {
    attach: function attach(context) {
      var $context = $(context);


      // $(document).on('load', function () {
        var bodyWidth = $("body").width();
        var bodyHeight = $("body").height();
        console.log(bodyHeight);

        if(bodyWidth<1380){
          $("#workspace").attr("height","92%");
        }else{
          $("#workspace").attr("height","95%");
        }

        $('.admin-main-left').css("min-height", bodyHeight-48);
        $('.admin-main-left').css("height", bodyHeight-48);
        $('.admincj_nav').css("height", bodyHeight-48);
        $('.admin-main-right').css("min-height", bodyHeight-48);
        $(window).resize(function(e) {
          bodyWidth = $("body").width();

          if(bodyWidth<1380){
            $("#workspace").attr("height","92%");
          }else{
            $("#workspace").attr("height","95%");
          }

          $('.admin-main-left').css("height", bodyHeight-48);
          $('.admin-main-right').css("height", bodyHeight-48);
        });
      // })
    }
  };
})(jQuery, Drupal, drupalSettings);
